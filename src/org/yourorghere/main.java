/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yourorghere;
import javax.media.opengl.GLCanvas;
 import java.awt.Frame;
 import java.awt.event.WindowAdapter;
 import java.awt.event.WindowEvent;
 import com.sun.opengl.util.Animator;
/**
 *
 * @author dairininya@gmail.com
 */

 
 public class main {     
     public static void main(String[] args) {
         main app = new main();
         app.createAndRun();
     }
 
     public void createAndRun() {
         Frame frame = new Frame("Jogl 3d Shape/Rotation");
         GLCanvas canvas = new GLCanvas();
         Animator animator = new Animator(canvas);
         animator.start();
         canvas.addGLEventListener(new Piraimde());
         frame.add(canvas);
         frame.setSize(640, 480);
         frame.setUndecorated(true);
         int size = frame.getExtendedState();
         size |= Frame.MAXIMIZED_BOTH;
         frame.setExtendedState(size);
 
         frame.addWindowListener(new WindowAdapter() {
             public void windowClosing(WindowEvent e) {
                 
                 System.exit(0);
             }
         });
         frame.setVisible(true);
         canvas.requestFocus();
     }
 }